======================
Linux Desktop for Rdio
======================
--------------------------------------
A minimal desktop application for Rdio
--------------------------------------

About
=====

Features:
 - a window for Rdio that doesn't get lost in your tabs
 - media keys

Not supported:
 - downloads
 - collection matching
 - mini player

Really, I just wanted media keys support.

Installation
============

Debian/Ubuntu:
  ``apt-get install gir1.2-webkit-3.0 gir1.2-javascriptcoregtk-3.0``

Icon
====
Available from: http://www.rdio.com/press/

.. image:: http://cdn3.rd.io/user/press/rdio-icon.png
   :height: 200px
   :target: http://cdn3.rd.io/user/press/rdio-icon.png

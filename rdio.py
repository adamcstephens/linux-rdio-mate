#!/usr/bin/env python

from gi.repository import GLib, Gio, GdkPixbuf, Gtk, Soup, JSCore, WebKit
import os

class RdioView(WebKit.WebView):
    def __init__(self):
        WebKit.WebView.__init__(self)

        # scale other content besides from text as well
        self.set_full_content_zoom(True)

        self.load_uri('http://www.rdio.com/')

class RdioWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_default_size(900, 700)
        self.set_title('Rdio')

        self.web_view = RdioView()

        self.scrolled_window = Gtk.ScrolledWindow()
        self.scrolled_window.add(self.web_view)
        self.scrolled_window.show_all()

        self.add(self.scrolled_window)

        self.web_view.connect('title-changed', self.title_changed)

        # TODO: async?
        icon_file = Gio.File.new_for_uri('http://ak.rdio.com/media//images/2/app_icons/rdio_256.png')
        icon = GdkPixbuf.Pixbuf.new_from_stream(icon_file.read(None), None)
        self.set_icon(icon)

        # Set up for media key handling
        bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
        self.proxy = Gio.DBusProxy.new_sync(bus, 0, None, 'org.mate.SettingsDaemon', '/org/mate/SettingsDaemon/MediaKeys', 'org.mate.SettingsDaemon.MediaKeys', None)
        self.proxy.call_sync('GrabMediaPlayerKeys', GLib.Variant('(su)', ('Rdio', 0)), 0, -1, None)
        self.proxy.connect('g-signal', self.mediakey_signal)

    def mediakey_signal(self, proxy, sender, signal, parameters):
        if signal != 'MediaPlayerKeyPressed':
            return
        key = parameters.get_child_value(1).get_string()
        if key == 'Play':
            self.web_view.execute_script('R.player.playPause()')
        if key == 'Next':
            self.web_view.execute_script('R.player.next()')
        if key == 'Previous':
            self.web_view.execute_script('R.player.previous()')

    def title_changed(self, view, frame, title):
        if frame == view.get_main_frame():
            self.set_title(title)




# make a data dir for this app
data_dir = os.path.join(GLib.get_user_data_dir(), 'linux-rdio')
if not os.path.exists(data_dir):
    os.makedirs(data_dir)

# put cookies in it
cookie_jar = os.path.join(data_dir, 'cookies.txt')
jar = Soup.CookieJarText(filename=cookie_jar)
session = WebKit.get_default_session()
session.add_feature(jar)


rw = RdioWindow()
rw.show()
rw.connect('destroy', lambda w: Gtk.main_quit())



Gtk.main()
